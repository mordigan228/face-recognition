import React, {Component} from 'react';
import Particles from 'react-particles-js';
import './App.css';
import Navigation from './components/Navigation/Navigation'
import Logo from './components/Logo/Logo'
import ImageLinkForm from './components/ImageLink/Form'
import FaceRecognition from './components/Face/Recog'
import Rank from './components/Rank/Rank'
import jopa from './background.json'
import Clarifai from 'clarifai';

const app = new Clarifai.App({apiKey: 'd173f894a3534ed68f7cd1c7dd8c54d4'});

class App extends Component {
  constructor() {
    super()
    this.state = {
      input: '',
      imageUrl: ''
    }
  }
  onInputChange = (e) => {
    this.setState({input: e.target.value})
  }
  onButtonSubmit = () => {
    this.setState({imageUrl: this.state.input})
    app
      .models
      .predict(Clarifai.FACE_DETECT_MODEL, this.state.input)
      .then(function (response) {
        console.log(response.outputs[0].data.regions[0].region_info.bounding_box)
      }, function (err) {
        console.log(err)
      });
  }
  render() {
    return (
      <div className="App">
        <Particles className="particles" params={jopa}/>
        <Navigation/>
        <Logo/>
        <Rank/>
        <ImageLinkForm
          onInputChange={this.onInputChange}
          onButtonSubmit={this.onButtonSubmit}/>
        <FaceRecognition imageUrl={this.state.imageUrl}/>
      </div>
    );
  }
}

export default App;
