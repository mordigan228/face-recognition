import React from 'react'

const FaceRecognition = ({imageUrl}) => {
  return (
    <div className="pistrus ma">
      <div className="absolute mt2">
        <img className="sizi" src={imageUrl} alt=""/>
      </div>
    </div>
  )
}
// http://fabriccreative.com/wp-content/uploads/2014/05/grace-face-1.jpg

export default FaceRecognition
