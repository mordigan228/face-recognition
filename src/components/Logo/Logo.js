import React from 'react'
import Tilt from 'react-tilt'
import './logo.css'
import neuron from './neuron.svg'

const Logo = () => {
  return (
    <div className="ma4 mt0">
      <Tilt
        className="Tilt"
        options={{
        max: 55
      }}
        style={{
        height: 150,
        width: 150
      }}>
        <div className="Tilt-inner">
          {/*<div>Icons made by
            <a href="https://www.flaticon.com/authors/darius-dan" title="Darius Dan">Darius Dan</a>
            from
            <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
            is licensed by
            <a
              href="http://creativecommons.org/licenses/by/3.0/"
              title="Creative Commons BY 3.0"
              target="_blank">CC 3.0 BY</a>
          </div>*/}
          <img src={neuron} alt="neuron svg made by darius-dan at www.flaticon.com"/>
        </div>
      </Tilt>
    </div>
  )
}

export default Logo
