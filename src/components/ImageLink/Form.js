import React from 'react'
import './form.css'

const ImageLinkForm = ({onInputChange, onButtonSubmit}) => {
  return (
    <div className="">
      <p className="f3">Lets find faces</p>
      <div className="center">
        <div className="pa4 br3 shadow-5">
          <input className="f4 pa2 w-70 center" type='text' onChange={onInputChange}/>
          <button
            className="w30 grow f4 link ph3 pv2 dib white bg-light-purple"
            onClick={onButtonSubmit}>Find</button>
        </div>
      </div>
    </div>
  )
}
export default ImageLinkForm
